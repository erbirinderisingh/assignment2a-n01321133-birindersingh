﻿<%@ Page Title=".NET" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Net.aspx.cs" Inherits="StudyGuide.Net" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <img src="images/nt.png">
            <h2>Study Guide for .Net</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Tricky Concept</h3>
                </div>
                <div class="panel-body">
                    <p>ASP.NET master pages allow you to create a consistent layout for the pages in your application. A single master page defines the look and feel and standard behavior that you want for all of the pages (or a group of pages) in your application. You can then create individual content pages that contain the content you want to display. When users request the content pages, they merge with the master page to produce output that combines the layout of the master page with the content from the content page.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Code Snippet</h3>
                </div>
                <div class="panel-body">
                    <asp:DataGrid ID="Code_MyCode" runat="server" CssClass="code" GridLines="None" Width="100%" CellPadding="1" >
                    <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                    <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
                    </asp:DataGrid>
                <!--
                    <pre>
                        public Shepherd(string n, int a)
                        {
                            name = n;
                            age = a;
                            canbark = true;
                            gooddog = true;
                            breed = "Shepherd";
                        }
                    </pre>
                -->
                    
                </div>
            </div>
        </div>
     </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Found this</h3>
                </div>
                <div class="panel-body">
                    <h3>Implicit inheritance</h3>
                    <p>Besides any types that they may inherit from through single inheritance, all types in the .NET type system implicitly inherit from Object or a type derived from it. The common functionality of Object is available to any type.To see what implicit inheritance means, let's define a new class, SimpleClass, that is simply an empty class definition:</p>
                     <asp:DataGrid ID="Code_FoundCode" runat="server" CssClass="code" GridLines="None" Width="100%" CellPadding="1" >
                    <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                    <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
                    </asp:DataGrid>
                    <!--
                    <pre>
                        public class SimpleClass
                        {
                            
                        }
                    </pre>
                    -->
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Further Reading</h3>
                </div>
                <div class="panel-body">
                    <ul>
                        <li>https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/inheritance</li>
                        <li>https://learn.humber.ca</li>
                    </ul>
                </div>
            </div>
        </div>
     </div>
</asp:Content>
